package main

import (
	"log"

	"git.sindominio.net/sindominio/lowry/db"
	"git.sindominio.net/sindominio/lowry/gitea"
	"git.sindominio.net/sindominio/lowry/ldap"
	"git.sindominio.net/sindominio/lowry/mail"
	"git.sindominio.net/sindominio/lowry/server"
	"github.com/namsral/flag"
)

func main() {
	var (
		ldapaddr          = flag.String("ldapaddr", "localhost:389", "LDAP server address and port")
		domain            = flag.String("domain", "", "LDAP domain components")
		ldappass          = flag.String("ldappass", "", "Password of the LDAP `admin' user")
		homepath          = flag.String("homepath", "/home/", "Path to the user homes")
		smtpaddr          = flag.String("smtpaddr", "localhost:25", "The address of the smtp server to send email")
		email             = flag.String("email", "", "The email address to send notifications from")
		emailpass         = flag.String("emailpass", "", "The password of the email address")
		httpaddr          = flag.String("httpaddr", ":8080", "Web server address and port")
		dbpath            = flag.String("dbpath", "bolt.db", "The path to store the lowry status database")
		ro                = flag.Bool("ro", false, "Read-Only mode")
		noLockUsers       = flag.Bool("noLockUsers", false, "Don't lock users (mostly useful for development)")
		giteaURL          = flag.String("giteaURL", "", "Gitea server address")
		token             = flag.String("token", "", "Gitea admin token")
		cloneAddr         = flag.String("cloneAddr", "", "Template repo address to copy")
		webhookRepoSecret = flag.String("webhookRepoSecret", "", "Webhook secret of the created repo")
		webhookRepoURL    = flag.String("webhookRepoURL", "", "Webhook url of the created repo")
		webhookURL        = flag.String("webhookURL", "", "Webhook URL to send user keys")
		webhookSecret     = flag.String("webhookSecret", "", "Webhook Secret to send user keys")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/lowry.conf", "Path to configuration file")
	flag.Parse()

	m := mail.Init(*email, *emailpass, *smtpaddr, *domain)
	l := ldap.Ldap{
		Addr:     *ldapaddr,
		Domain:   *domain,
		Pass:     *ldappass,
		HomePath: *homepath,
		RO:       *ro,
	}
	err := l.Init()
	if err != nil {
		log.Fatal(err)
	}
	g := gitea.Init(*giteaURL, *token, *cloneAddr, *webhookRepoURL, *webhookRepoSecret, *webhookURL, *webhookSecret)

	ldb, err := db.Init(*dbpath)
	if err != nil {
		log.Fatal(err)
	}
	defer ldb.Close()

	s := server.New(&l, m, ldb, g, *domain)
	go s.Cleanup(*noLockUsers)
	log.Fatal(s.Serve(*httpaddr))
}
