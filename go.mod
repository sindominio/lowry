module git.sindominio.net/sindominio/lowry

go 1.12

require (
	code.gitea.io/sdk/gitea v0.11.3
	github.com/ProtonMail/go-crypto v0.0.0-20220407094043-a94812496cf5 // indirect
	github.com/ProtonMail/gopenpgp/v2 v2.4.6
	github.com/go-asn1-ber/asn1-ber v1.4.1 // indirect
	github.com/go-ldap/ldap/v3 v3.1.8
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/namsral/flag v1.7.4-pre
	github.com/tv42/zbase32 v0.0.0-20220222190657-f76a9fc892fa
	go.etcd.io/bbolt v1.3.4
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
