module git.sindominio.net/sindominio/lowry

go 1.19

require (
	code.gitea.io/sdk/gitea v0.16.0
	github.com/ProtonMail/gopenpgp/v2 v2.7.4
	github.com/go-ldap/ldap/v3 v3.4.6
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/securecookie v1.1.2
	github.com/gorilla/sessions v1.2.2
	github.com/namsral/flag v1.7.4-pre
	github.com/tv42/zbase32 v0.0.0-20220222190657-f76a9fc892fa
	go.etcd.io/bbolt v1.3.8
	golang.org/x/crypto v0.15.0
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20221128193559-754e69321358 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20230923063757-afb1ddc0824c // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/cloudflare/circl v1.3.6 // indirect
	github.com/davidmz/go-pageant v1.0.2 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.5 // indirect
	github.com/go-fed/httpsig v1.1.0 // indirect
	github.com/google/uuid v1.4.0 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
