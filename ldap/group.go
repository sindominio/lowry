package ldap

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/go-ldap/ldap/v3"
)

// Group has the ldap data of the group
type Group struct {
	DN          string
	Name        string
	GID         int
	Description string
	Members     []string
	OpenPGPkey  *OpenPGPkey
}

// InGroup checks if user is part of group
func (l Ldap) InGroup(user string, group string) bool {
	if user == "" {
		return false
	}

	conn, err := l.connect()
	if err != nil {
		return false
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		l.groupDN(group),
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectClass=posixGroup)(memberUid=%s))", ldap.EscapeFilter(user)),
		[]string{"dn"},
		nil,
	)
	sr, err := conn.Search(searchRequest)
	return err == nil && len(sr.Entries) > 0
}

// GetGroup returns the group matching the name
func (l Ldap) GetGroup(name string) (Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(cn=%s))", ldap.EscapeFilter(name))
	groups, err := l.searchGroup(filter)
	if err != nil {
		return Group{}, err
	}
	if len(groups) == 0 {
		return Group{}, ErrNotFound
	}
	return groups[0], nil

}

// GetGID returns the group matching the gid
func (l Ldap) GetGID(gid int) (Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(gidNumber=%d))", gid)
	groups, err := l.searchGroup(filter)
	if err != nil {
		return Group{}, err
	}
	if len(groups) == 0 {
		return Group{}, errors.New("Can't find group " + strconv.Itoa(gid))
	}
	return groups[0], nil
}

// ListGroups returns all groups in ldap with members
func (l Ldap) ListGroups() ([]Group, error) {
	filter := "(&(objectClass=posixGroup)(memberUid=*))"
	return l.searchGroup(filter)
}

// UserGroups returns a list of groups the user is member of
func (l Ldap) UserGroups(user string) ([]Group, error) {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(memberUid=%s))", ldap.EscapeFilter(user))
	return l.searchGroup(filter)
}

// AddGroup adds the group to ldap
func (l Ldap) AddGroup(group string, description string) error {
	if _, err := l.GetGroup(group); err == nil {
		return ErrAlreadyExist
	}

	gid, err := l.getLastID("gidNumber")
	if err != nil {
		return err
	}
	gid++

	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	dn := l.groupDN(group)
	addRequest := ldap.NewAddRequest(dn, nil)
	addRequest.Attribute("cn", []string{ldap.EscapeFilter(group)})
	addRequest.Attribute("objectClass", []string{"top", "posixGroup"})
	addRequest.Attribute("gidNumber", []string{strconv.Itoa(gid)})
	if description != "" {
		addRequest.Attribute("description", []string{ldap.EscapeFilter(description)})
	}
	return conn.Add(addRequest)
}

// UpdateGroupDescription set a new description for the group
func (l Ldap) UpdateGroupDescription(group string, description string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.groupDN(group), nil)
	if description == "" {
		modifyRequest.Replace("description", []string{})
	} else {
		modifyRequest.Replace("description", []string{ldap.EscapeFilter(description)})
	}
	return conn.Modify(modifyRequest)
}

// DelGroup removes the group in ldap
func (l Ldap) DelGroup(group string) error {
	return l.del(l.groupDN(group))
}

// EmptyGroup removes all the members from the group
func (l Ldap) EmptyGroup(group string) error {
	filter := fmt.Sprintf("(&(objectClass=posixGroup)(cn=%s))", ldap.EscapeFilter(group))
	groups, err := l.searchGroup(filter)
	if err != nil {
		return err
	}
	if len(groups) == 0 {
		return ErrNotFound
	}
	return l.delUsersGroup(groups[0].Members, group)
}

// AddUserGroup add user into the group members
func (l Ldap) AddUserGroup(user string, group string) error {
	u, err := l.GetUser(user)
	if err != nil {
		return err
	}
	if u.Locked != Unlocked {
		return ErrNotFound
	}

	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.groupDN(group), nil)
	modifyRequest.Add("memberUid", []string{ldap.EscapeFilter(user)})
	return conn.Modify(modifyRequest)
}

// DelUserGroup removes the user from the group members
func (l Ldap) DelUserGroup(user string, group string) error {
	return l.delUsersGroup([]string{ldap.EscapeFilter(user)}, group)
}

func (l Ldap) delUsersGroup(users []string, group string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.groupDN(group), nil)
	modifyRequest.Delete("memberUid", users)
	return conn.Modify(modifyRequest)
}

func (l Ldap) groupDN(group string) string {
	groupStr := ldap.EscapeFilter(group)
	return fmt.Sprintf("cn=%s,ou=group,%s", groupStr, l.DC)
}

func (l Ldap) searchGroup(filter string) ([]Group, error) {
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		"ou=group,"+l.DC,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		filter,
		append([]string{"dn", "cn", "memberUid", "gidNumber", "description"}, openPGPAttributes[:]...),
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}
	groups := []Group{}
	for _, entry := range sr.Entries {
		groups = append(groups, newGroup(entry))
	}
	return groups, nil
}

func newGroup(entry *ldap.Entry) Group {
	gid, _ := strconv.Atoi(entry.GetAttributeValue("gidNumber"))
	return Group{
		DN:          entry.DN,
		Name:        entry.GetAttributeValue("cn"),
		Members:     entry.GetAttributeValues("memberUid"),
		Description: entry.GetAttributeValue("description"),
		GID:         gid,
		OpenPGPkey:  openPGPkey(entry),
	}
}

// ChangeGroupOpenPGPkey updates or sets a new OpenPGPkey for the group
func (l Ldap) ChangeGroupOpenPGPkey(group string, fingerprint string, expiry time.Time, key []byte, wkdHash string) error {
	email := fmt.Sprintf("%s@%s", group, l.Domain)
	return l.changeOpenPGPkey(l.groupDN(group), fingerprint, expiry, key, wkdHash, email)
}
