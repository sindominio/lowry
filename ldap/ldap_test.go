package ldap

import "testing"

const (
	addr   = "localhost:389"
	domain = "nodomain"
	pass   = "password"
	home   = "/home/"
)

func TestInit(t *testing.T) {
	testLdap(t)
}

func testLdap(t *testing.T) *Ldap {
	l := Ldap{
		Addr:     addr,
		Domain:   domain,
		Pass:     pass,
		HomePath: home,
	}
	err := l.Init()
	if err != nil {
		t.Fatalf("Error on Init(): %v", err)
	}
	return &l
}
