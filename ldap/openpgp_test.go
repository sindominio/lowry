package ldap

import (
	"bytes"
	"testing"
	"time"
)

const (
	fingerprint = "AABBCCDDEEFF1122334455"
	wkdHash     = "hashhash"
)

var (
	key = []byte("openpgpkey")
)

func TestOpenPGPuser(t *testing.T) {
	l := testLdap(t)
	u, err := l.GetUser(user)
	if err != nil {
		t.Errorf("GetUser() failed: %v", err)
	}
	if u.OpenPGPkey != nil {
		t.Errorf("user already has a key")
	}

	dn := l.userDN(user)
	err = l.changeOpenPGPkey(dn, fingerprint, time.Time{}, key, wkdHash, "")
	if err != nil {
		t.Errorf("ChangeOpenPGPkey() failed: %v", err)
	}

	u, err = l.GetUser(user)
	if err != nil {
		t.Errorf("GetUser() failed: %v", err)
	}
	if u.OpenPGPkey == nil {
		t.Fatal("user doesn't have a key")
	}
	if u.OpenPGPkey.Fingerprint != fingerprint {
		t.Errorf("fingeprint doesn't match: %s", u.OpenPGPkey.Fingerprint)
	}
	if !bytes.Equal(u.OpenPGPkey.Key, key) {
		t.Errorf("key doesn't match: %s", u.OpenPGPkey.Key)
	}
	if u.OpenPGPkey.WkdHash != wkdHash {
		t.Errorf("wkdHash doesn't match: %s", u.OpenPGPkey.WkdHash)
	}
	if !u.OpenPGPkey.Expiry.IsZero() {
		t.Errorf("expiry is not zero: %v", u.OpenPGPkey.Expiry)
	}

	err = l.DeleteOpenPGPkey(dn)
	if err != nil {
		t.Errorf("DeleteOpenPGPkey() failed: %v", err)
	}

	u, err = l.GetUser(user)
	if err != nil {
		t.Errorf("GetUser() failed: %v", err)
	}
	if u.OpenPGPkey != nil {
		t.Errorf("user already has a key")
	}
}

func TestOpenPGPgroup(t *testing.T) {
	l := testLdap(t)
	g, err := l.GetGroup(group)
	if err != nil {
		t.Errorf("GetGRoup() failed: %v", err)
	}
	if g.OpenPGPkey != nil {
		t.Errorf("user already has a key")
	}

	dn := l.groupDN(group)
	err = l.changeOpenPGPkey(dn, fingerprint, time.Time{}, key, wkdHash, group+"@nodomain")
	if err != nil {
		t.Errorf("ChangeOpenPGPkey() failed: %v", err)
	}

	g, err = l.GetGroup(group)
	if err != nil {
		t.Errorf("GetGRoup() failed: %v", err)
	}
	if g.OpenPGPkey == nil {
		t.Fatal("user doesn't have a key")
	}
	if g.OpenPGPkey.Fingerprint != fingerprint {
		t.Errorf("fingeprint doesn't match: %s", g.OpenPGPkey.Fingerprint)
	}
	if !bytes.Equal(g.OpenPGPkey.Key, key) {
		t.Errorf("key doesn't match: %s", g.OpenPGPkey.Key)
	}
	if g.OpenPGPkey.WkdHash != wkdHash {
		t.Errorf("wkdHash doesn't match: %s", g.OpenPGPkey.WkdHash)
	}
	if !g.OpenPGPkey.Expiry.IsZero() {
		t.Errorf("expiry is not zero: %v", g.OpenPGPkey.Expiry)
	}

	err = l.DeleteOpenPGPkey(dn)
	if err != nil {
		t.Errorf("DeleteOpenPGPkey() failed: %v", err)
	}

	g, err = l.GetGroup(group)
	if err != nil {
		t.Errorf("GetGRoup() failed: %v", err)
	}
	if g.OpenPGPkey != nil {
		t.Errorf("user already has a key")
	}
}

func TestUpdateOpenPGP(t *testing.T) {
	l := testLdap(t)
	u, err := l.GetUser(user)
	if err != nil {
		t.Errorf("GetUser() failed: %v", err)
	}
	if u.OpenPGPkey != nil {
		t.Errorf("user already has a key")
	}

	dn := l.userDN(user)
	err = l.changeOpenPGPkey(dn, fingerprint, time.Time{}, key, wkdHash, "")
	if err != nil {
		t.Errorf("ChangeOpenPGPkey() failed: %v", err)
	}

	err = l.changeOpenPGPkey(dn, fingerprint, time.Time{}, key, wkdHash, "")
	if err != nil {
		t.Errorf("ChangeOpenPGPkey() failed: %v", err)
	}

	err = l.DeleteOpenPGPkey(dn)
	if err != nil {
		t.Errorf("DeleteOpenPGPkey() failed: %v", err)
	}
}
