package ldap

import (
	"errors"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
)

var openPGPAttributes = []string{"openPGPKey", "openPGPId", "openPGPExpiry", "openPGPKeyHash"}

type OpenPGPkey struct {
	Fingerprint string
	Expiry      time.Time
	Key         []byte
	WkdHash     string
}

func (l Ldap) changeOpenPGPkey(dn string, fingerprint string, expiry time.Time, key []byte, wkdHash string, email string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(dn, nil)
	_, err = l.getOpenPGPKey(dn, conn)
	if err != nil {
		if errors.Is(err, ErrNotFound) {
			modifyRequest.Add("objectClass", []string{"openPGP"})
		} else {
			return err
		}
	}
	modifyRequest.Replace("openPGPId", []string{fingerprint})
	modifyRequest.Replace("openPGPExpiry", []string{expiry.Format(dateFormat)})
	modifyRequest.Replace("openPGPKey", []string{string(key)})
	modifyRequest.Replace("openPGPKeyHash", []string{wkdHash})
	if email != "" {
		modifyRequest.Replace("mail", []string{email})
	}
	return conn.Modify(modifyRequest)
}

func (l Ldap) DeleteOpenPGPkey(dn string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(dn, nil)
	modifyRequest.Delete("objectClass", []string{"openPGP"})
	modifyRequest.Delete("openPGPId", []string{})
	modifyRequest.Delete("openPGPExpiry", []string{})
	modifyRequest.Delete("openPGPKey", []string{})
	modifyRequest.Delete("openPGPKeyHash", []string{})
	if strings.Contains(strings.ToLower(dn), "ou=group") {
		modifyRequest.Delete("mail", []string{})
	}
	return conn.Modify(modifyRequest)
}

func (l Ldap) getOpenPGPKey(dn string, conn *ldap.Conn) (*OpenPGPkey, error) {
	searchRequest := ldap.NewSearchRequest(
		dn,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		"((objectClass=openPGP))",
		openPGPAttributes,
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}
	switch len(sr.Entries) {
	case 1:
		entry := sr.Entries[0]
		return openPGPkey(entry), nil
	case 0:
		return nil, ErrNotFound
	default:
		return nil, errors.New("More than one user found!!!")
	}
}

func openPGPkey(entry *ldap.Entry) *OpenPGPkey {
	openPGPexpiry, _ := time.Parse(dateFormat, entry.GetAttributeValue("openPGPExpiry"))
	key := OpenPGPkey{
		Fingerprint: entry.GetAttributeValue("openPGPId"),
		Expiry:      openPGPexpiry,
		Key:         []byte(entry.GetAttributeValue("openPGPKey")),
		WkdHash:     entry.GetAttributeValue("openPGPKeyHash"),
	}

	if len(key.Key) == 0 || key.Fingerprint == "" || key.WkdHash == "" {
		return nil
	}
	return &key
}
