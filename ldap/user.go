package ldap

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
)

const (
	dateFormat = "20060102150405Z"
)

var searchAttributes = append([]string{"dn", "uid", "uidNumber", "gidNumber", "loginShell", "homeDirectory", "mail", "authTimestamp", "sdRole", "sdLocked", "userPassword"}, openPGPAttributes[:]...)

//User has the ldap data of the user
type User struct {
	DN         string
	Name       string
	Shell      string
	Home       string
	Mail       string
	UID        int
	GID        int
	Role       Role
	Locked     Locked
	OpenPGPkey *OpenPGPkey
	LastLogin  time.Time
}

// ValidateUser in the ldap
func (l Ldap) ValidateUser(user string, pass string) (User, error) {
	if err := l.deletedUser(user); err != nil {
		return User{}, err
	}

	conn, err := l.login(user, pass)
	if err != nil {
		return User{}, err
	}
	conn.Close()

	conn, err = l.connect()
	if err != nil {
		return User{}, err
	}
	defer conn.Close()

	entry, err := l.searchUser(user, conn)
	if err != nil {
		return User{}, err
	}
	return newUser(entry), nil
}

func (l Ldap) deletedUser(user string) error {
	u, err := l.GetUser(user)
	if err == nil && u.Locked == Deleted {
		err = fmt.Errorf("Deleted user %s", user)
	}
	return err

}

// IsUserPassUptodate will be true if the password for that user in ldap is using the latest crypto
func (l Ldap) IsUserPassUptodate(user string) bool {
	conn, err := l.connect()
	if err != nil {
		return false
	}
	defer conn.Close()

	entry, err := l.searchUser(user, conn)
	if err != nil {
		return false
	}
	return strings.ToLower(entry.GetAttributeValue("userPassword")[:10]) == "{crypt}$6$"
}

// ChangePass changes logged in user's password
func (l Ldap) ChangePass(user string, oldpass string, newpass string) error {
	if oldpass == "" {
		return errors.New("Old password can not be empty")
	}

	conn, err := l.login(user, oldpass)
	if err != nil {
		return err
	}
	defer conn.Close()
	return l.changePass(conn, user, oldpass, newpass)
}

// ChangePassAdmin changes user's password as admin
// (without knowing the old password)
func (l Ldap) ChangePassAdmin(user string, pass string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()
	return l.changePass(conn, user, "", pass)
}

func (l Ldap) changePass(conn *ldap.Conn, user, oldpass, newpass string) error {
	if l.RO {
		log.Println("Changing password in read only mode")
		return nil
	}
	passwordModifyRequest := ldap.NewPasswordModifyRequest(l.userDN(user), oldpass, newpass)
	_, err := conn.PasswordModify(passwordModifyRequest)
	return err
}

// GetUserRole returns the role of the user
func (l Ldap) GetUserRole(user string) Role {
	conn, err := l.connect()
	if err != nil {
		log.Printf("Error connecting to ldap: %v", err)
		return Undefined
	}
	defer conn.Close()

	entry, err := l.searchUser(user, conn)
	if err != nil {
		log.Printf("Error searching for user in ldap: %v", err)
		return Undefined
	}
	return RoleFromString(entry.GetAttributeValue("sdRole"))
}

//GetUser returns the user data
func (l Ldap) GetUser(name string) (User, error) {
	conn, err := l.connect()
	if err != nil {
		return User{}, err
	}
	defer conn.Close()

	entry, err := l.searchUser(name, conn)
	if err != nil {
		return User{}, err
	}
	return newUser(entry), nil
}

//ListUsers returns a list of all users in the ldap
func (l Ldap) ListUsers() ([]User, error) {
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(
		"ou=People,"+l.DC,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		"(&(objectClass=posixAccount))",
		searchAttributes,
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	users := []User{}
	for _, entry := range sr.Entries {
		users = append(users, newUser(entry))
	}
	return users, nil
}

// AddUser to the ldap
func (l *Ldap) AddUser(user string, pass string, gid int) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	entry, err := l.searchUser(user, conn)
	if entry != nil {
		return ErrAlreadyExist
	}

	uid, err := l.getLastID("uidNumber")
	if err != nil {
		return err
	}
	uid++

	dn := l.userDN(user)
	addRequest := ldap.NewAddRequest(dn, nil)
	addRequest.Attribute("uid", []string{ldap.EscapeFilter(user)})
	addRequest.Attribute("cn", []string{ldap.EscapeFilter(user)})
	addRequest.Attribute("sn", []string{ldap.EscapeFilter(user)})
	addRequest.Attribute("objectClass", []string{"inetOrgPerson", "posixAccount", "shadowAccount", "sdPerson", "top"})
	addRequest.Attribute("uidNumber", []string{strconv.Itoa(uid)})
	addRequest.Attribute("gidNumber", []string{strconv.Itoa(gid)})
	addRequest.Attribute("loginShell", []string{"/bin/false"})
	addRequest.Attribute("homeDirectory", []string{l.HomePath + user})
	addRequest.Attribute("mail", []string{user + "@" + l.MailDomain})
	addRequest.Attribute("sdRole", []string{"amiga"})
	err = conn.Add(addRequest)
	if err != nil {
		return err
	}

	passwordModifyRequest := ldap.NewPasswordModifyRequest(dn, "", pass)
	_, err = conn.PasswordModify(passwordModifyRequest)
	return err

}

// DelUser removes the user from ldap
func (l Ldap) DelUser(user string) error {
	return l.del(l.userDN(user))
}

// ChangeShell for the user
func (l Ldap) ChangeShell(user, shell string) error {
	return l.changeUser(user, "loginShell", []string{shell})
}

// ChangeRole for the user
func (l Ldap) ChangeRole(user string, role Role) error {
	return l.changeUser(user, "sdRole", []string{role.String()})
}

// ChangeLocked for the user
func (l Ldap) ChangeLocked(user string, locked Locked) error {
	if locked != Unlocked {
		return l.changeUser(user, "sdLocked", []string{locked.String()})
	}

	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.userDN(user), nil)
	modifyRequest.Delete("sdLocked", []string{})
	return conn.Modify(modifyRequest)
}

// ChangeUserOpenPGPkey updates or sets a new OpenPGPkey for the user
func (l Ldap) ChangeUserOpenPGPkey(user string, fingerprint string, expiry time.Time, key []byte, wkdHash string) error {
	return l.changeOpenPGPkey(l.userDN(user), fingerprint, expiry, key, wkdHash, "")
}

func (l Ldap) changeUser(user, attribute string, value []string) error {
	conn, err := l.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	modifyRequest := ldap.NewModifyRequest(l.userDN(user), nil)
	modifyRequest.Replace(attribute, value)
	return conn.Modify(modifyRequest)
}

func (l Ldap) userDN(user string) string {
	userStr := ldap.EscapeFilter(user)
	return fmt.Sprintf("uid=%s,ou=People,%s", userStr, l.DC)
}

func (l Ldap) login(user string, password string) (*ldap.Conn, error) {
	conn, err := l.connect()
	if err != nil {
		return nil, err
	}
	entry, err := l.searchUser(user, conn)
	if err != nil {
		conn.Close()
		return nil, err
	}
	userdn := entry.DN
	return conn, conn.Bind(userdn, password)
}

func (l Ldap) searchUser(user string, conn *ldap.Conn) (entry *ldap.Entry, err error) {
	searchRequest := ldap.NewSearchRequest(
		"ou=People,"+l.DC,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&(objectClass=posixAccount)(uid=%s))", ldap.EscapeFilter(user)),
		searchAttributes,
		nil,
	)
	sr, err := conn.Search(searchRequest)
	if err != nil {
		return entry, err
	}

	switch len(sr.Entries) {
	case 1:
		entry = sr.Entries[0]
		return entry, nil
	case 0:
		return entry, ErrNotFound
	default:
		return entry, errors.New("More than one user found!!!")
	}
}

func newUser(entry *ldap.Entry) User {
	uid, _ := strconv.Atoi(entry.GetAttributeValue("uidNumber"))
	gid, _ := strconv.Atoi(entry.GetAttributeValue("gidNumber"))
	lastLogin, _ := time.Parse(dateFormat, entry.GetAttributeValue("authTimestamp"))
	return User{
		DN:         entry.DN,
		Name:       entry.GetAttributeValue("uid"),
		Shell:      entry.GetAttributeValue("loginShell"),
		Home:       entry.GetAttributeValue("homeDirectory"),
		Mail:       entry.GetAttributeValue("mail"),
		UID:        uid,
		GID:        gid,
		Role:       RoleFromString(entry.GetAttributeValue("sdRole")),
		Locked:     LockedFromString(entry.GetAttributeValue("sdLocked")),
		OpenPGPkey: openPGPkey(entry),
		LastLogin:  lastLogin,
	}
}
