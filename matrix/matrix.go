package matrix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type Matrix struct {
	matrixURL  string
	token      string
	serverName string
}

func Init(matrixURL, token, serverName string) *Matrix {
	return &Matrix{matrixURL, token, serverName}
}

// https://spec.matrix.org/legacy/client_server/r0.6.1#get-matrix-client-r0-admin-whois-userid
// https://matrix-org.github.io/synapse/develop/admin_api/user_admin_api.html#query-current-sessions-for-a-user
type whois struct {
	Devices map[string]deviceInfo `json:"devices"`
}

type deviceInfo struct {
	Sessions []sessionInfo `json:"sessions"`
}

type sessionInfo struct {
	Connections []connectionInfo `json:"connections"`
}

type connectionInfo struct {
	LastSeen int64 `json:"last_seen"`
}

func (m Matrix) LastSeen(user string) (time.Time, error) {
	tzero := time.UnixMilli(0)
	res, err := m.httpRequest("GET", "whois/"+m.userID(user), nil)
	if err != nil {
		return tzero, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return tzero, fmt.Errorf("Get error: %s", res.Status)
	}

	var w whois
	dec := json.NewDecoder(res.Body)
	err = dec.Decode(&w)
	if err != nil {
		return tzero, err
	}

	var lastSeen int64
	for _, device := range w.Devices {
		for _, session := range device.Sessions {
			for _, connection := range session.Connections {
				if connection.LastSeen > lastSeen {
					lastSeen = connection.LastSeen
				}
			}
		}
	}

	return time.UnixMilli(lastSeen), nil
}

type deactivate struct {
	Erase bool `json:"erase"`
}

func (m Matrix) DeleteUser(user string) error {
	// deactivate the user
	// https://matrix-org.github.io/synapse/develop/admin_api/user_admin_api.html#deactivate-account
	res, err := m.httpRequest("POST", "deactivate/"+m.userID(user), deactivate{true})
	if err != nil {
		return err
	}
	if res.StatusCode != 200 {
		return fmt.Errorf(res.Status)
	}

	// delete media
	// https://matrix-org.github.io/synapse/develop/admin_api/user_admin_api.html#delete-media-uploaded-by-a-user
	res, err = m.httpRequest("DELETE", "users/"+m.userID(user)+"/media", nil)
	if err != nil {
		return err
	}
	if res.StatusCode != 200 {
		return fmt.Errorf(res.Status)
	}
	return nil
}

func (m Matrix) userID(user string) string {
	return fmt.Sprintf("@%s:%s", user, m.serverName)
}

func (m Matrix) httpRequest(method, path string, body any) (*http.Response, error) {
	url := fmt.Sprintf("%s/_synapse/admin/v1/%s", m.matrixURL, path)
	var bodyReader io.Reader
	if body != nil {
		bbody, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		bodyReader = bytes.NewReader(bbody)
	}

	req, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+m.token)
	return http.DefaultClient.Do(req)
}
