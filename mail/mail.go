package mail

import (
	"bytes"
	"net/smtp"
	"strings"
	"text/template"
)

// Mail holds the mail sender object
type Mail struct {
	auth     smtp.Auth
	smtpaddr string
	fromAddr string
	domain   string
	tmpl     *template.Template
}

// Init mail sender
func Init(email, password, smtpaddr, domain string) *Mail {
	tmpl := template.Must(template.ParseFiles(
		"tmpl/wellcome.mail",
		"tmpl/openpgp_expire.mail",
	))
	hostname := strings.Split(smtpaddr, ":")[0]
	username := strings.Split(email, "@")[0]
	return &Mail{
		auth:     smtp.PlainAuth("", username, password, hostname),
		smtpaddr: smtpaddr,
		fromAddr: email,
		domain:   domain,
		tmpl:     tmpl,
	}
}

type tmplData struct {
	To   string
	From string
	Data interface{}
}

// Send mail to recipients using a mail template
func (m Mail) Send(to []string, template string, data interface{}) error {
	for i, recipient := range to {
		if !strings.Contains(recipient, "@") {
			to[i] += "@" + m.domain
		}
	}
	tmplD := tmplData{
		To:   strings.Join(to, ", "),
		From: m.fromAddr,
		Data: data}
	var buff bytes.Buffer
	err := m.tmpl.ExecuteTemplate(&buff, template+".mail", tmplD)
	if err != nil {
		return err
	}
	return smtp.SendMail(m.smtpaddr, m.auth, m.fromAddr, to, buff.Bytes())
}
