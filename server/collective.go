package server

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"git.sindominio.net/sindominio/lowry/ldap"
	"github.com/gorilla/mux"
)

const (
	maxCollectives = 4
)

func (s *Server) addCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("add_collective", w, r)
	ok, err := s.canCreateCollective(response)
	if err != nil {
		log.Println("An error ocurred checking if can create collective: ", err)
		s.errorHandler(w, r)
		return
	}

	status := ""
	if !ok {
		status = "quota"
	}
	response.execute(status)
}

func (s *Server) postAddCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("add_collective", w, r)
	ok, err := s.canCreateCollective(response)
	if err != nil {
		log.Println("An error ocurred checking if can create collective: ", err)
		s.errorHandler(w, r)
		return
	}

	if !ok {
		response.execute("quota")
		return
	}

	collectiveName := r.FormValue("collective")
	if !validUserName(collectiveName) {
		log.Println("Can't create collective ", collectiveName, ": invalid name")
		response.execute("invalid")
		return
	}
	description := r.FormValue("description")
	err = s.ldap.AddGroup(collectiveName, description)
	if err != nil {
		if errors.Is(err, ldap.ErrAlreadyExist) {
			response.execute("exists")
		} else {
			log.Println("An error ocurred adding collective '", collectiveName, "': ", err)
			s.errorHandler(w, r)
		}
		return
	}

	err = s.ldap.AddUserGroup(response.User, collectiveName)
	if err != nil {
		log.Println("An error ocurred adding user ", response.User, " to collective '", collectiveName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/collective/"+collectiveName, http.StatusFound)
}

func (s *Server) delCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	collectiveName := r.FormValue("collective")

	response := s.newResponse("", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, collectiveName) {
		log.Println(response.User, "don't have rights to delete", collectiveName, "collective")
		s.forbiddenHandler(w, r)
		return
	}

	err := s.ldap.EmptyGroup(collectiveName)
	if err != nil {
		log.Println("An error ocurred deleting collective '", collectiveName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/collectives/mine/", http.StatusFound)
}

func (s *Server) collectivesHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("collectives", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to list collectives")
		s.forbiddenHandler(w, r)
		return
	}

	collectives, err := s.ldap.ListGroups()
	if err != nil {
		log.Println("An error ocurred retrieving collective list: ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(collectives)
}

func (s *Server) userCollectivesHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("collectives", w, r)
	if response.User == "" {
		log.Println("Attemp to list own collective without being logged")
		s.forbiddenHandler(w, r)
		return
	}

	collectives, err := s.ldap.UserGroups(response.User)
	if err != nil {
		log.Println("An error ocurred retrieving collective list: ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(collectives)
}

func (s *Server) collectiveHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collectiveName := vars["name"]
	response := s.newResponse("collective", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, collectiveName) {
		log.Println("Attemp to list collective without rights")
		s.forbiddenHandler(w, r)
		return
	}

	collective, err := s.ldap.GetGroup(collectiveName)
	if err != nil {
		log.Println("An error ocurred retrieving collective '", collectiveName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	response.execute(collective)
}

func (s *Server) addUserCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collectiveName := vars["name"]
	response := s.newResponse("error", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, collectiveName) {
		log.Println("Attemp to add user to collective without rights")
		s.forbiddenHandler(w, r)
		return
	}

	user := r.FormValue("user")
	isInCollective, err := s.isUserinCollective(user, collectiveName)
	if err != nil {
		log.Println("An error ocurred getting collective ", collectiveName, "to add", user, ":", err)
		s.errorHandler(w, r)
		return
	}
	if isInCollective {
		log.Println("User", user, "already colelctive ", collectiveName, "don't need to add them")
	} else {

		err := s.ldap.AddUserGroup(user, collectiveName)
		if err != nil {
			if errors.Is(err, ldap.ErrNotFound) {
				response.execute(ErrorData{
					Title:       "Cuenta no encontrada",
					Description: fmt.Sprintf("No hay ninguna cuenta con el nombre %s para añadirla al colectivo %s. Comprueba que tengas el nombre bien e intentalo de nuevo.", user, collectiveName),
					BackURL:     "/collective/" + collectiveName,
				})
			} else {
				log.Println("An error ocurred adding user '", user, "' to ", collectiveName, ": ", err)
				s.errorHandler(w, r)
			}
			return
		}
	}
	http.Redirect(w, r, "/collective/"+collectiveName, http.StatusFound)
}

func (s *Server) delUserCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collectiveName := vars["name"]
	response := s.newResponse("", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, collectiveName) {
		log.Println("Attemp to del user to collective without rights")
		s.forbiddenHandler(w, r)
		return
	}

	user := r.FormValue("user")
	err := s.ldap.DelUserGroup(user, collectiveName)
	if err != nil {
		log.Println("An error ocurred del user '", user, "' to ", collectiveName, ": ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/collective/"+collectiveName, http.StatusFound)
}

func (s *Server) descriptionCollectiveHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	collectiveName := vars["name"]
	response := s.newResponse("", w, r)
	if !response.IsAdmin && !s.ldap.InGroup(response.User, collectiveName) {
		log.Println("Attemp to edit description without rights of collective", collectiveName)
		s.forbiddenHandler(w, r)
		return
	}

	description := r.FormValue("description")
	err := s.ldap.UpdateGroupDescription(collectiveName, description)
	if err != nil {
		log.Println("An error ocurred to set description to ", collectiveName, ": ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/collective/"+collectiveName, http.StatusFound)
}

func (s *Server) canCreateCollective(r responseT) (bool, error) {
	if r.User == "" {
		return false, nil
	}
	if r.Role == ldap.Sindominante {
		return true, nil
	}

	count, err := s.db.CountCollectives(r.User)
	return count <= maxCollectives, err
}

func (s *Server) isUserinCollective(user, collective string) (bool, error) {
	group, err := s.ldap.GetGroup(collective)
	if err != nil {
		return false, err
	}
	found := false
	for _, member := range group.Members {
		if member == user {
			found = true
			break
		}
	}
	return found, nil
}
