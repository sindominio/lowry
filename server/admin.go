package server

import (
	"log"
	"net/http"
	"sort"

	"git.sindominio.net/sindominio/lowry/ldap"
	"github.com/gorilla/mux"
)

func (s *Server) usersHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("users", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	users, err := s.ldap.ListUsers()
	if err != nil {
		log.Println("An error ocurred retrieving user list: ", err)
		s.errorHandler(w, r)
		return
	}
	sort.Slice(users, func(i, j int) bool {
		return users[i].Locked < users[j].Locked
	})
	response.execute(users)
}

func (s *Server) userHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userName := vars["name"]
	response := s.newResponse("user", w, r)
	if !response.IsAdmin && userName != response.User {
		log.Println("Non admin attemp to list users")
		s.forbiddenHandler(w, r)
		return
	}

	user, err := s.ldap.GetUser(userName)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	mainGroup, err := s.ldap.GetGID(user.GID)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "' main group: ", err)
	}
	groups, err := s.ldap.UserGroups(userName)
	if err != nil {
		log.Println("An error ocurred retrieving user '", userName, "' groups: ", err)
	}
	data := struct {
		User      ldap.User
		MainGroup ldap.Group
		Groups    []ldap.Group
	}{user, mainGroup, groups}
	response.execute(data)
}

func (s *Server) roleHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user role")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	roleStr := r.FormValue("role")
	role := ldap.RoleFromString(roleStr)
	if role == ldap.Undefined {
		log.Println("Not valid role '", roleStr, "' for ", userName)
		s.errorHandler(w, r)
		return
	}

	err := s.ldap.ChangeRole(userName, role)
	if err != nil {
		log.Println("An error ocurred changing role of '", userName, "' to", roleStr, ": ", err)
		s.errorHandler(w, r)
		return
	}

	user, err := s.ldap.GetUser(userName)
	if err != nil {
		log.Println("Error fetching user '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}

	if user.Shell == "/bin/false" && role == ldap.Sindominante {
		err := s.ldap.ChangeShell(userName, "/bin/bash")
		if err != nil {
			log.Println("An error ocurred changing shell of '", userName, "': ", err)
			s.errorHandler(w, r)
			return
		}
	}

	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}

func (s *Server) lockHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user locked")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	lockedStr := r.FormValue("locked")
	locked := ldap.LockedFromString(lockedStr)
	if locked == ldap.Unknown {
		log.Println("Not valid locked status '", lockedStr, "' for ", userName)
		s.errorHandler(w, r)
		return
	}

	err := s.ldap.ChangeLocked(userName, locked)
	if err != nil {
		log.Println("An error ocurred changing locked of '", userName, "' to", lockedStr, ": ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}

func (s *Server) passwdadmHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user password")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	pass := r.FormValue("password")
	err := s.ldap.ChangePassAdmin(userName, pass)
	if err != nil {
		log.Println("An error ocurred changing password of '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}

func (s *Server) shellHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("", w, r)
	if !response.IsAdmin {
		log.Println("Non admin attemp to change user password")
		s.forbiddenHandler(w, r)
		return
	}

	vars := mux.Vars(r)
	userName := vars["name"]
	shell := r.FormValue("shell")
	err := s.ldap.ChangeShell(userName, shell)
	if err != nil {
		log.Println("An error ocurred changing shell of '", userName, "': ", err)
		s.errorHandler(w, r)
		return
	}
	http.Redirect(w, r, "/users/"+userName, http.StatusFound)
}
