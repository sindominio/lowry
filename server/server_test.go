package server

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	"git.sindominio.net/sindominio/lowry/db"
	"git.sindominio.net/sindominio/lowry/ldap"
	"git.sindominio.net/sindominio/lowry/matrix"
)

const (
	addr   = "localhost:389"
	domain = "nodomain"
	pass   = "password"
	home   = "/home/"
	key    = "key"
	value  = "value"
)

func testServer(t *testing.T, matrixURL string) (s Server, closeServer func()) {
	s.domain = domain
	s.ldap = &ldap.Ldap{
		Addr:     addr,
		Domain:   domain,
		Pass:     pass,
		HomePath: home,
	}
	err := s.ldap.Init()
	if err != nil {
		t.Fatalf("Error on Init(): %v", err)
	}

	s.matrix = matrix.Init(matrixURL, "", domain)
	s.db, closeServer = initTestDB(t)
	return
}

func initTestDB(t *testing.T) (d *db.DB, closeDB func()) {
	dir, err := ioutil.TempDir("", "lowry_test")
	if err != nil {
		t.Fatalf("Can't create a temp dir: %v", err)
	}
	d, err = db.Init(path.Join(dir, "bolt.db"))
	if err != nil {
		t.Fatalf("Can't init db (%s): %v", dir, err)
	}
	closeDB = func() {
		d.Close()
		os.RemoveAll(dir)
	}
	return
}
