package server

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"git.sindominio.net/sindominio/lowry/ldap"
	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/gorilla/mux"
	"github.com/tv42/zbase32"
)

var (
	ErrInvalidKey = errors.New("Key is not valid")
	ErrNoKeyId    = errors.New("The key doesn't contain a valid user id")
	ErrExpiredKey = errors.New("The key is expired")
)

type openPGPdata struct {
	Error       string
	Success     bool
	Key         string
	Fingerprint string
	Expiry      string
	Collective  string
}

func (s *Server) openPGPkeyHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("openpgp", w, r)
	if response.User == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	vars := mux.Vars(r)
	collective := vars["name"]

	var data openPGPdata
	if r.Method == "POST" {
		formKey := r.FormValue("key")
		name := response.User
		if collective != "" {
			if isInCollective, _ := s.isUserinCollective(response.User, collective); !isInCollective {
				log.Println("User", response.User, "doesn't have permissions to modify the openPGP key of", collective)
				s.errorHandler(w, r)
				return
			}
			name = collective
		}
		err := s.changeOpenPGPkey(name, formKey, collective != "")
		if err != nil {
			switch err {
			case ErrInvalidKey:
				data.Error = "invalid"
			case ErrNoKeyId:
				data.Error = "no-identity"
			case ErrExpiredKey:
				data.Error = "expired"
			default:
				log.Println("Error setting the openpgp key into ldap:", err)
				s.errorHandler(w, r)
				return
			}
			data.Key = formKey
		} else {
			data.Success = true
		}
	}

	var key *ldap.OpenPGPkey
	if collective != "" {
		collectiveData, err := s.ldap.GetGroup(collective)
		if err != nil {
			log.Println("Error loading", collective, "profile:", err)
			s.errorHandler(w, r)
			return
		}
		key = collectiveData.OpenPGPkey
		data.Collective = collective
	} else {
		userData, err := s.ldap.GetUser(response.User)
		if err != nil {
			log.Println("Error loading", response.User, "profile:", err)
			s.errorHandler(w, r)
			return
		}
		key = userData.OpenPGPkey
	}

	if key != nil {
		if data.Key == "" {
			var err error
			data.Key, err = getArmoredKey(key.Key)
			if err != nil {
				log.Println("Error getting", response.User, "armored key:", err)
				s.errorHandler(w, r)
				return
			}
		}

		data.Fingerprint = key.Fingerprint
		if !key.Expiry.IsZero() {
			data.Expiry = key.Expiry.Format("02/01/2006")
		}
	}

	response.execute(data)
}

func (s *Server) changeOpenPGPkey(name, formKey string, collective bool) error {
	key, err := crypto.NewKeyFromArmored(formKey)
	if err != nil {
		log.Println("Can't parse key for", name, ":", err)
		return ErrInvalidKey
	}
	fingerprint := strings.ToUpper(key.GetFingerprint())

	email := name + "@" + s.domain
	found := false
	for _, identity := range key.GetEntity().Identities {
		if identity.UserId.Email == email {
			found = true
			break
		}
	}
	if !found {
		log.Println("User email", email, "not in key", fingerprint)
		return ErrNoKeyId
	}

	pubKey, err := key.GetPublicKey()
	if err != nil {
		return err
	}

	expiry := getEntityExpiry(key)
	if !expiry.IsZero() && expiry.Before(time.Now()) {
		return ErrExpiredKey
	}
	wkdHash := fmt.Sprintf("%s@%s", calculateWKDhash(name), s.domain)

	if collective {
		err = s.ldap.ChangeGroupOpenPGPkey(name, fingerprint, expiry, pubKey, wkdHash)
	} else {
		err = s.ldap.ChangeUserOpenPGPkey(name, fingerprint, expiry, pubKey, wkdHash)
	}
	return err
}

func getArmoredKey(key []byte) (string, error) {
	okey, err := crypto.NewKey(key)
	if err != nil {
		return "", err
	}

	return okey.GetArmoredPublicKey()

}

func getEntityExpiry(key *crypto.Key) time.Time {
	ent := key.GetEntity()
	i := ent.PrimaryIdentity()
	if i.SelfSignature.KeyLifetimeSecs != nil && *i.SelfSignature.KeyLifetimeSecs > 0 {
		return ent.PrimaryKey.CreationTime.Add(
			time.Duration(*i.SelfSignature.KeyLifetimeSecs) * time.Second)
	}

	// Key does not expire.
	return time.Time{}
}

func calculateWKDhash(user string) string {
	local := strings.ToLower(user)
	hashedLocal := sha1.Sum([]byte(local))
	return zbase32.EncodeToString(hashedLocal[:])
}
