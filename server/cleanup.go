package server

import (
	"log"
	"math"
	"time"

	"git.sindominio.net/sindominio/lowry/ldap"
)

var (
	inviteExpireDuration     = time.Hour * 24 * 30     // 30 days
	collectiveExpireDuration = time.Hour * 24 * 90     // 90 days
	accountExpireDuration    = time.Hour * 24 * 90     // 90 days
	accountBlockDuration     = time.Hour * 24 * 6 * 30 // ~ 6 months
	accountDeleteDuration    = time.Hour * 24 * 365    // ~ 1 year
	notifyKeyExpiredDuration = time.Hour * 24 * 30     // 30 days
)

// Cleanup runs periodic clean up tasks
func (s *Server) Cleanup(noLockUsers bool) {
	for {
		users, err := s.ldap.ListUsers()
		if err != nil {
			log.Printf("Error listing users for updating: %v", err)
		} else {
			for _, u := range users {
				if !noLockUsers {
					s.updateUserLock(u)
				}
				s.checkKeyExpiration(u.DN, u.OpenPGPkey, u.Name, u.Mail)
			}
		}

		collectives, err := s.ldap.ListGroups()
		if err != nil {
			log.Printf("Error listing collectives for updating: %v", err)
		} else {
			for _, c := range collectives {
				mail := c.Name + "@" + s.domain
				s.checkKeyExpiration(c.DN, c.OpenPGPkey, c.Name, mail)
			}
		}

		s.expireDBEntries()
		time.Sleep(time.Minute * 61)
	}
}

func (s *Server) updateUserLock(u ldap.User) {
	if u.Shell == "/bin/false" && u.Role == ldap.Sindominante {
		err := s.ldap.ChangeShell(u.Name, "/bin/bash")
		if err != nil {
			log.Println("An error ocurred changing shell of '", u.Name, "': ", err)
		}
	}

	if u.Locked == ldap.Deleted {
		// if the account is deleted we don't need to check for a status update
		return
	}

	newLocked := ldap.Unknown
	lastSeen := u.LastLogin
	matrixLastSeen, err := s.matrix.LastSeen(u.Name)
	if err != nil {
		log.Println("Error fetching last seen", u.Name, "in matrix:", err)
		return
	}
	if matrixLastSeen.After(lastSeen) {
		lastSeen = matrixLastSeen
	}
	if dbLastSeen, err := s.db.GetLastSeen(u.Name); err == nil && dbLastSeen.After(lastSeen) {
		lastSeen = dbLastSeen
	}
	sinceLastSeen := time.Now().Sub(lastSeen)

	if u.Locked != ldap.Deleted && sinceLastSeen > accountDeleteDuration {
		newLocked = ldap.Deleted
		s.deleteUser(u.Name)
	} else if u.Locked != ldap.Blocked && sinceLastSeen > accountBlockDuration && sinceLastSeen < accountDeleteDuration {
		newLocked = ldap.Blocked
		if lastSeen != u.LastLogin {
			// all sessions will be deleted in matrix when the account gets blocked
			// lets update the lastseen in the database
			s.db.AddLastSeen(u.Name, lastSeen)
		}
	} else {
		return
	}

	err = s.ldap.ChangeLocked(u.Name, newLocked)
	if err != nil {
		log.Printf("Error changing locked to %s for user %s: %v", newLocked.String(), u.Name, err)
	}
	if u.Role == ldap.Sindominante {
		err = s.ldap.ChangeRole(u.Name, ldap.Amiga)
		if err != nil {
			log.Printf("Error changing role for blocked user %s: %v", u.Name, err)
		}
	}
}

func (s *Server) deleteUser(user string) {
	err := s.matrix.DeleteUser(user)
	if err != nil {
		log.Println("Error deleting matrix", user, ":", err)
	}
}

type NotificationData struct {
	Name        string
	Fingerprint string
	Days        int
}

func (s *Server) checkKeyExpiration(dn string, key *ldap.OpenPGPkey, name string, mail string) {
	if key == nil {
		return
	}

	if key.Expiry.IsZero() {
		return
	}

	if key.Expiry.Before(time.Now()) {
		s.ldap.DeleteOpenPGPkey(dn)

		data := NotificationData{
			Name:        name,
			Fingerprint: key.Fingerprint,
			Days:        0,
		}
		s.mail.Send([]string{mail}, "openpgp_expire", data)
	}

	notifiedFingerprint, err := s.db.GetOpenpgpNotification(dn)
	if err != nil {
		log.Printf("An error has occurred accessing the user %s last notification: %v", name, err)
	}
	if time.Now().Add(notifyKeyExpiredDuration).After(key.Expiry) && notifiedFingerprint != key.Fingerprint {
		data := NotificationData{
			Name:        name,
			Fingerprint: key.Fingerprint,
			Days:        int(math.Round(key.Expiry.Sub(time.Now()).Hours() / 24)),
		}
		s.mail.Send([]string{mail}, "openpgp_expire", data)

		err = s.db.AddOpenpgpNotification(dn, key.Fingerprint)
		if err != nil {
			log.Printf("An error has occurred storing user %s last notification: %v", name, err)
		}
	}
}

func (s *Server) expireDBEntries() {
	s.db.ExpireInvites(inviteExpireDuration)
	s.db.ExpireAccounts(accountExpireDuration)
	s.db.ExpireCollectives(collectiveExpireDuration)
	s.db.ExpireOpenpgpNotifications(time.Hour + notifyKeyExpiredDuration)
	s.db.ExpireLastSeen(accountDeleteDuration)
}
