package server

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"git.sindominio.net/sindominio/lowry/ldap"
)

const (
	user = "user"
)

var lastSeen time.Time

func whois(w http.ResponseWriter, req *http.Request) {
	userid := strings.TrimPrefix(req.URL.Path, "/_synapse/admin/v1/whois/")
	_, err := fmt.Fprintf(w, `{
    "user_id": "%s",
    "devices": {
        "": {
            "sessions": [
                {
                    "connections": [
                        {
                            "ip": "1.2.3.4",
                            "last_seen": %d,
                            "user_agent": "Mozilla/5.0 ..."
                        }
                    ]
                }
            ]
        }
    }
}`, userid, lastSeen.UnixMilli())
	if err != nil {
		fmt.Println("Found error on whois response:", err)
	}
}

func TestLockMatrixLastSeen(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(whois))
	defer ts.Close()
	s, closeServer := testServer(t, ts.URL)
	defer closeServer()

	u, err := s.ldap.GetUser(user)
	if err != nil {
		t.Fatal(err)
	}
	if u.Locked != ldap.Unlocked {
		t.Error("User is already locked")
	}

	lastSeen = time.Now()
	u.LastLogin = time.Time{}
	s.updateUserLock(u)
	u, err = s.ldap.GetUser(user)
	if err != nil {
		t.Fatal(err)
	}
	if u.Locked != ldap.Unlocked {
		t.Error("User is", u.Locked, "but lastseen is now")
	}

	lastSeen = time.Now().Add(-(accountBlockDuration + 1))
	u.LastLogin = time.Time{}
	s.updateUserLock(u)
	u, err = s.ldap.GetUser(user)
	if err != nil {
		t.Fatal(err)
	}
	if u.Locked != ldap.Blocked {
		t.Error("User is", u.Locked, " when we expected blocked by an old lastseen")
	}

	lastSeen = time.Time{}
	u.LastLogin = time.Time{}
	s.updateUserLock(u)
	u, err = s.ldap.GetUser(user)
	if err != nil {
		t.Fatal(err)
	}
	if u.Locked != ldap.Blocked {
		t.Error("User is", u.Locked, "ignoring the DB")
	}

	err = s.ldap.ChangeLocked(user, ldap.Unlocked)
	if err != nil {
		t.Fatal(err)
	}
}
