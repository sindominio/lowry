package server

import (
	"git.sindominio.net/sindominio/lowry/gitea"
	"log"
	"net/http"
)

func (s *Server) giteaHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("gitea", w, r)

	// check permission for showing page
	if response.Shell == "/bin/false" {
		log.Printf("Web deploy module: User %s doesn't have shell, can't access gitea", response.User)
		response := s.newResponse("403", w, r)
		response.execute(nil)
		return
	}

	// first load
	if r.Method != "POST" {
		response.execute("")
		return
	}

	// data for html template
	type template_data struct {
		ExitStatus string
		RepoName   string
	}
	data := template_data{
		ExitStatus: "",
		RepoName:   "",
	}

	// init vars
	usergitea := response.User
	reponame := r.FormValue("reponame")
	folder := r.FormValue("folder") // "userfolder" or "usersubfolder"
	mode := r.FormValue("mode")     // "creterepo" or "setfolder"
	keypub := ""
	keypriv := ""

	// check form (TODO: js)
	if len(reponame) == 0 {
		data.ExitStatus = "err_repo_empty"
		response.execute(data)
		return
	}

	// init repo object (config vars)
	repo, err := s.gitea.Repo(usergitea, reponame)
	if err != nil {
		log.Printf("Web deploy module: Error loading config for gitea functionality: %v", err)
		data.ExitStatus = "err_lowry_config"
		response.execute(data)
		return
	}

	// check user exists, and return uid.
	uid, err := repo.UserID()
	if err != nil {
		// connection error?
		data.ExitStatus = "err_gitea_connection"
		response.execute(data)
		return
	}
	if uid == -2 {
		log.Printf("Web deploy module: User doesn't exist on gitea.")
		data.ExitStatus = "err_gitea_user"
		response.execute(data)
		return
	}
	// GITEA TASKS (if create is selected)
	if mode == "createrepo" {

		// clean existing repo
		if repo.Exists() {
			if err := repo.Delete(); err != nil {
				log.Printf("Web deploy module: Return, repo is empty.")
				data.ExitStatus = "err_gitea_connection"
				response.execute(data)
				return
			}
		}

		// create repo from template repo
		if err = repo.Migrate(); err != nil {
			log.Printf("Web deploy module: Error migrating (copying) template repository: %v", err)
			data.ExitStatus = "err_gitea_connection"
			response.execute(data)
			return
		}

		// generate keys
		keypriv, keypub, err = gitea.GenerateRSAKeyPair(4096)
		if err != nil {
			log.Printf("Web deploy module: Error generating key pair: %v", err)
			if err := repo.Delete(); err != nil {
				log.Printf("Web deploy module: Error deleting gitea repository: %v", err)
			}
			data.ExitStatus = "err_gitea_connection"
			response.execute(data)
			return
		}

		// create deploy keys on repo
		if err = repo.DeployKey(keypub); err != nil {
			log.Printf("Web deploy module: Error deploying key on gitea repository: %v", err)
			if err := repo.Delete(); err != nil {
				log.Printf("Web deploy module: Error deleting gitea repository: %v", err)
			}
			data.ExitStatus = "err_gitea_connection"
			response.execute(data)
			return
		}

		// create gitea webhook on repo
		if err = repo.CreateWebhook(); err != nil {
			log.Printf("Web deploy module: Error creating webhook on repository: %v", err)
			if err := repo.Delete(); err != nil {
				log.Printf("Web deploy module: Error deleting repository: %v", err)
			}
			data.ExitStatus = "err_gitea_connection"
			response.execute(data)
			return
		}

		// gitea tasks completed
		log.Printf("Web deploy module: Repo created, keys added, webhook added.")
		data.ExitStatus = "repocreated-" + folder

	} else if mode == "setfolder" {
		if !repo.Exists() {
			data.ExitStatus = "repo-nonexistent"
			response.execute(data)
			return
		} else {
			data.ExitStatus = folder
		}
	}

	log.Printf("Web deploy module: Set %s", folder)

	// get clone url from created repo to send it to deploy webhook
	cloneurl, err := repo.GetSSHURL()
	if err != nil {
		log.Printf("Web deploy module: Error getting info for new repo: %v", err)
		data.ExitStatus = "err_gitea_connection"
		response.execute(data)
		return
	}

	// WEB HOST TASKS - send keys and set folder option via webhook.
	if err = s.gitea.WebhookDeploy(usergitea, reponame, keypriv, keypub, cloneurl, data.ExitStatus); err != nil {
		log.Printf("Web deploy module: Error, web host unreachable: %v", err)
		if err := repo.Delete(); err != nil {
			log.Printf("Web deploy module: Error deleting repository: %v", err)
		}
		data.ExitStatus = "err_webhook_connection"
		response.execute(data)
		return
	}

	// Success: return reponame
	data.RepoName = reponame

	// success
	log.Printf("Web deploy module: keys and/or folder option was send to web host.")
	log.Printf("Web deploy module: Done!")
	response.execute(data)

}
