package server

import (
	"log"
	"net/http"

	"git.sindominio.net/sindominio/lowry/ldap"
)

func (s *Server) homeHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("index", w, r)
	if response.User == "" {
		response = s.newResponse("login", w, r)
		response.execute(false)
		return
	}

	needPasswordChange := !s.ldap.IsUserPassUptodate(response.User)
	data := struct {
		NeedPasswordChange bool
	}{needPasswordChange}
	response.execute(data)
}

func (s *Server) loginHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("user")
	pass := r.FormValue("password")

	user, err := s.ldap.ValidateUser(name, pass)
	if err != nil {
		log.Printf("Error login %s: %v", name, err)
		response := s.newResponse("login", w, r)
		response.execute(true)
		return
	}
	switch user.Locked {
	case ldap.Unlocked:
		s.sess.set(name, w, r)
		http.Redirect(w, r, "/", http.StatusFound)
	case ldap.Blocked:
		s.sess.setBlocked(name, w, r)
		response := s.newResponse("blocked_notice", w, r)
		response.execute(true)
	default:
		response := s.newResponse("login", w, r)
		response.execute(true)
	}
}

func (s *Server) logoutHandler(w http.ResponseWriter, r *http.Request) {
	s.sess.del(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *Server) passwordHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("password", w, r)
	if response.User == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.Method != "POST" {
		response.execute("")
		return
	}

	oldpass := r.FormValue("oldpass")
	pass := r.FormValue("password")
	pass2 := r.FormValue("password2")

	if pass != pass2 {
		response.execute("WrongPass")
		return
	}
	if pass == oldpass {
		response.execute("SameOldPass")
		return
	}
	if !validPassword(pass) {
		response.execute("invalidPass")
		return
	}

	err := s.ldap.ChangePass(response.User, oldpass, pass)
	if err != nil {
		response.execute("WrongOldpass")
	} else {

		response.execute("PassChanged")
	}
}

func (s *Server) unlockHandler(w http.ResponseWriter, r *http.Request) {
	session := s.sess.get(w, r)
	if session != nil && session.blockedUser != "" {
		err := s.ldap.ChangeLocked(session.blockedUser, ldap.Unlocked)
		if err != nil {
			log.Printf("Error unlocking %s: %v", session.blockedUser, err)
		}
		s.sess.set(session.blockedUser, w, r)
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
