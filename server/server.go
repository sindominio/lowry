package server

import (
	"html/template"
	"net/http"

	"git.sindominio.net/sindominio/lowry/db"
	"git.sindominio.net/sindominio/lowry/gitea"
	"git.sindominio.net/sindominio/lowry/ldap"
	"git.sindominio.net/sindominio/lowry/mail"
	"git.sindominio.net/sindominio/lowry/matrix"
	"github.com/gorilla/mux"
)

type Server struct {
	ldap   *ldap.Ldap
	mail   *mail.Mail
	db     *db.DB
	sess   *sessionStore
	tmpl   *template.Template
	gitea  *gitea.Gitea
	matrix *matrix.Matrix
	domain string
}

func New(l *ldap.Ldap, m *mail.Mail, ldb *db.DB, g *gitea.Gitea, mtx *matrix.Matrix, domain string) *Server {
	return &Server{
		ldap:   l,
		mail:   m,
		db:     ldb,
		sess:   initSessionStore(),
		tmpl:   initTemplate(),
		gitea:  g,
		matrix: mtx,
		domain: domain,
	}
}

// Serve lowry web site
func (s *Server) Serve(addr string) error {
	r := mux.NewRouter()
	r = r.StrictSlash(true)
	var notFoundFunc http.HandlerFunc
	notFoundFunc = s.notFoundHandler
	r.NotFoundHandler = notFoundFunc

	r.HandleFunc("/", s.homeHandler)
	r.HandleFunc("/login/", s.loginHandler)
	r.HandleFunc("/logout/", s.logoutHandler).Methods("POST")
	r.HandleFunc("/password/", s.passwordHandler)
	r.HandleFunc("/openpgp/", s.openPGPkeyHandler)

	r.HandleFunc("/users/", s.usersHandler)
	r.HandleFunc("/users/{name}", s.userHandler)
	r.HandleFunc("/users/{name}/role/", s.roleHandler).Methods("POST")
	r.HandleFunc("/users/{name}/password/", s.passwdadmHandler).Methods("POST")
	r.HandleFunc("/users/{name}/shell/", s.shellHandler).Methods("POST")
	r.HandleFunc("/users/{name}/lock/", s.lockHandler).Methods("POST")

	r.HandleFunc("/unlock/", s.unlockHandler)
	r.HandleFunc("/invites/", s.listInvitesHandler)
	r.HandleFunc("/invites/{invite}/del/", s.deleteInviteHandler)
	r.HandleFunc("/adduser/", s.createInviteHandler)
	r.HandleFunc("/adduser/{invite}", s.addUserHandler)

	r.HandleFunc("/collectives/", s.collectivesHandler)
	r.HandleFunc("/collectives/mine/", s.userCollectivesHandler)
	r.HandleFunc("/collectives/add/", s.addCollectiveHandler).Methods("GET")
	r.HandleFunc("/collectives/add/", s.postAddCollectiveHandler).Methods("POST")
	r.HandleFunc("/collectives/del/", s.delCollectiveHandler).Methods("POST")
	r.HandleFunc("/collective/{name}", s.collectiveHandler)
	r.HandleFunc("/collective/{name}/add/", s.addUserCollectiveHandler).Methods("POST")
	r.HandleFunc("/collective/{name}/del/", s.delUserCollectiveHandler).Methods("POST")
	r.HandleFunc("/collective/{name}/description/", s.descriptionCollectiveHandler).Methods("POST")
	r.HandleFunc("/collective/{name}/openpgp/", s.openPGPkeyHandler)

	r.HandleFunc("/gitea/", s.giteaHandler)

	r.Handle("/{file}", http.FileServer(http.Dir("dist")))
	r.Handle("/img/{file}", http.StripPrefix("/img/", http.FileServer(http.Dir("dist/img"))))
	r.Handle("/js/{file}", http.StripPrefix("/js/", http.FileServer(http.Dir("dist/js"))))

	return http.ListenAndServe(addr, r)
}

func (s *Server) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	response := s.newResponse("404", w, r)
	response.execute(nil)
}

func (s *Server) forbiddenHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusForbidden)
	response := s.newResponse("403", w, r)
	response.execute(nil)
}

func (s *Server) errorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	response := s.newResponse("500", w, r)
	response.execute(nil)
}
