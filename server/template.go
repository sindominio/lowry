package server

import (
	"html/template"
	"log"
	"net/http"

	"git.sindominio.net/sindominio/lowry/ldap"
)

type responseT struct {
	User    string
	IsAdmin bool
	Role    ldap.Role
	Shell   string
	Section string
	Data    interface{}
	w       http.ResponseWriter
	r       *http.Request
	tmpl    *template.Template
}

type ErrorData struct {
	Title       string
	Description string
	BackURL     string
}

func initTemplate() *template.Template {
	return template.Must(template.ParseFiles(
		"tmpl/403.html",
		"tmpl/404.html",
		"tmpl/500.html",
		"tmpl/error.html",
		"tmpl/header.html",
		"tmpl/header_close.html",
		"tmpl/extra_script_password_validator.html",
		"tmpl/footer.html",
		"tmpl/navbar.html",
		"tmpl/login.html",
		"tmpl/index.html",
		"tmpl/passwd-tips.html",
		"tmpl/password.html",
		"tmpl/user.html",
		"tmpl/users.html",
		"tmpl/blocked_notice.html",
		"tmpl/invite.html",
		"tmpl/invites.html",
		"tmpl/adduser.html",
		"tmpl/adduser_success.html",
		"tmpl/collective.html",
		"tmpl/collectives.html",
		"tmpl/add_collective.html",
		"tmpl/gitea.html",
		"tmpl/openpgp.html",
		"tmpl/load-password.js",
	))
}

func (s *Server) newResponse(template string, w http.ResponseWriter, r *http.Request) responseT {
	session := s.sess.get(w, r)
	user := ""
	role := ldap.Undefined
	shell := ""
	admin := false
	if session != nil {
		u, err := s.ldap.GetUser(session.user)
		if err != nil {
			log.Printf("Error fetching user %s: %v", user, err)
		} else {
			user = session.user
			admin = s.isAdmin(user)
			role = u.Role
			shell = u.Shell
		}
	}
	return responseT{
		User:    user,
		IsAdmin: admin,
		Role:    role,
		Shell:   shell,
		Section: template,
		Data:    nil,
		w:       w,
		r:       r,
		tmpl:    s.tmpl,
	}
}

func (r *responseT) execute(data interface{}) {
	r.Data = data
	if err := r.tmpl.ExecuteTemplate(r.w, r.Section+".html", r); err != nil {
		log.Println("An error ocurred loading template '", r.Section, "': ", err)
		r.tmpl.ExecuteTemplate(r.w, "500.html", r)
	}
}

func (s *Server) isAdmin(user string) bool {
	return s.ldap.InGroup(user, "adm")
}
