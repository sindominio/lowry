package db

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

const (
	key   = "key"
	value = "value"
)

func initTestDB(t *testing.T) *DB {
	dir, err := ioutil.TempDir("", "lowry_test")
	if err != nil {
		t.Fatalf("Can't create a temp dir: %v", err)
	}
	db, err := Init(path.Join(dir, "bolt.db"))
	if err != nil {
		t.Fatalf("Can't init db (%s): %v", dir, err)
	}
	return db
}

func delTestDB(db *DB) {
	dir := path.Dir(db.bolt.Path())
	db.Close()
	os.RemoveAll(dir)
}

func TestPutGet(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	var v string
	err := db.get(inviteBucket, key, &v)
	if _, ok := err.(notFoundError); !ok {
		t.Errorf("Got something else than notFoundError before put: %v", err)
	}

	err = db.put(inviteBucket, key, value)
	if err != nil {
		t.Fatalf("Got an error putting: %v", err)
	}

	err = db.get(inviteBucket, key, &v)
	if err != nil {
		t.Fatalf("Got an error getting: %v", err)
	}
	if v != value {
		t.Fatalf("Expected %v got %v", value, v)
	}

	err = db.del(inviteBucket, key)
	if err != nil {
		t.Fatalf("Got an error deleting: %v", err)
	}
	err = db.get(inviteBucket, key, value)
	if _, ok := err.(notFoundError); !ok {
		t.Errorf("Got something else than notFoundError after delete: %v", err)
	}
}
