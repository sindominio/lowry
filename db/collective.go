package db

import (
	"encoding/json"
	"errors"
	"log"
	"time"

	"go.etcd.io/bbolt"
)

var (
	collectiveBucket = []byte("collective")
)

type collective struct {
	Name         string
	CreationDate time.Time
}

// AddCollective stores in the db the collective that was created by host
func (db *DB) AddCollective(name string, host string) error {
	var collectives []collective
	err := db.get(collectiveBucket, host, &collectives)
	if err != nil && !errors.Is(err, notFoundError{}) {
		return err
	}

	collectives = append(collectives, collective{name, time.Now()})
	return db.put(collectiveBucket, host, collectives)
}

// CountCollectives returns the nubmer of collectives created by host
func (db *DB) CountCollectives(host string) (int, error) {
	var collectives []collective
	err := db.get(collectiveBucket, host, &collectives)
	if errors.Is(err, notFoundError{}) {
		return 0, nil
	}
	return len(collectives), err
}

// ExpireCollectives older than duration
func (db *DB) ExpireCollectives(duration time.Duration) error {
	return db.bolt.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(collectiveBucket)
		return b.ForEach(func(k, v []byte) error {
			var collectives []collective
			err := json.Unmarshal(v, &collectives)
			if err != nil {
				log.Printf("Error unmarshalling %s: %v", string(k), err)
				return nil
			}

			newCollectives := []collective{}
			for _, c := range collectives {
				if c.CreationDate.Add(duration).After(time.Now()) {
					newCollectives = append(newCollectives, c)
				}
			}
			if len(newCollectives) == len(collectives) {
				return nil
			}

			encodedValue, err := json.Marshal(newCollectives)
			if err != nil {
				return err
			}
			return b.Put(k, encodedValue)
		})
	})
}
