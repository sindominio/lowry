package db

import (
	"testing"
	"time"
)

const (
	code = "code"
)

func TestAddInvite(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	if db.IsInviteValid(code) {
		t.Errorf("Got valid invite before adding it")
	}

	err := db.AddInvite(code, "")
	if err != nil {
		t.Fatalf("Got an error adding invite: %v", err)
	}
	if !db.IsInviteValid(code) {
		t.Errorf("Got invalid invite after adding it")
	}

	err = db.DelInvite(code)
	if err != nil {
		t.Fatalf("Got an error deleting invite: %v", err)
	}
	if db.IsInviteValid(code) {
		t.Errorf("Got valid invite deleting it")
	}
}

func TestExpireInvites(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	err := db.AddInvite(code, "")
	if err != nil {
		t.Fatalf("Got an error adding invite: %v", err)
	}
	if !db.IsInviteValid(code) {
		t.Errorf("Got invalid invite after adding it")
	}

	err = db.ExpireInvites(time.Microsecond)
	if err != nil {
		t.Fatalf("Got an error expiring invites: %v", err)
	}
	if db.IsInviteValid(code) {
		t.Errorf("Got valid invite after expiring it")
	}
}

func TestListUserInvites(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	err := db.AddInvite(code, "user")
	if err != nil {
		t.Fatalf("Got an error adding invite: %v", err)
	}
	invites, err := db.ListUserInvites("user")
	if err != nil {
		t.Fatalf("Got an error listing invites: %v", err)
	}
	if len(invites) != 1 {
		t.Fatalf("Unexpected number of invites: %v", invites)
	}
	if invites[0].Code != code {
		t.Fatalf("Code not in list of user invites: %v", invites)
	}
}
