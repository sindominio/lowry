package db

import (
	"errors"
	"time"
)

var (
	openpgpNotificationsBucket = []byte("openpgp-notifications")
)

type openpgpNotification struct {
	Fingerprint  string
	CreationDate time.Time
}

// AddOpenpgpNotification stores in the db the dn being notified for their key being expired
func (db *DB) AddOpenpgpNotification(dn string, fingerprint string) error {
	return db.put(openpgpNotificationsBucket, dn, openpgpNotification{fingerprint, time.Now()})
}

// GetOpenpgpNotification gets the fingerprint latest notification for the dn
func (db *DB) GetOpenpgpNotification(dn string) (string, error) {
	var notif openpgpNotification
	err := db.get(openpgpNotificationsBucket, dn, &notif)
	if errors.Is(err, notFoundError{}) {
		err = nil
	}
	return notif.Fingerprint, err
}

// ExpireOpenpgpNotifications older than duration
func (db *DB) ExpireOpenpgpNotifications(duration time.Duration) error {
	return db.expire(openpgpNotificationsBucket, duration)
}
