package db

import (
	"time"
)

var (
	lastseenBucket = []byte("lastseen")
)

type lastseen struct {
	Date time.Time
}

// AddLastSeen stores in the db the account and it's lastseen date
func (db *DB) AddLastSeen(user string, lastSeen time.Time) error {
	return db.put(lastseenBucket, user, lastseen{lastSeen})
}

// GetLastSeen time for the user
func (db *DB) GetLastSeen(user string) (time.Time, error) {
	var lastSeen lastseen
	err := db.get(lastseenBucket, user, &lastSeen)
	return lastSeen.Date, err
}

// ExpireLastSeen older than duration
func (db *DB) ExpireLastSeen(duration time.Duration) error {
	return db.expire(lastseenBucket, duration)
}
