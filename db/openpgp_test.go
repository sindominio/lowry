package db

import (
	"testing"
	"time"
)

const (
	user        = "user"
	fingerprint = "AABBCCDDEEFF1122334455"
)

func TestAddOpenPGPNotification(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	fp, err := db.GetOpenpgpNotification(user)
	if err != nil {
		t.Fatalf("Got an error getting openpgp notification: %v", err)
	}
	if fp != "" {
		t.Errorf("Got an unexpected fingerprint: %s", fp)
	}

	err = db.AddOpenpgpNotification(user, fingerprint)
	if err != nil {
		t.Fatalf("Got an error adding a openpgp notification: %v", err)
	}

	fp, err = db.GetOpenpgpNotification(user)
	if err != nil {
		t.Fatalf("Got an error getting openpgp notification: %v", err)
	}
	if fp != fingerprint {
		t.Errorf("Got an unexpected fingerprint: %s", fp)
	}
}

func TestExpireOpenpgpNofications(t *testing.T) {
	db := initTestDB(t)
	defer delTestDB(db)

	err := db.AddOpenpgpNotification(user, fingerprint)
	if err != nil {
		t.Fatalf("Got an error adding a openpgp notification: %v", err)
	}

	fp, err := db.GetOpenpgpNotification(user)
	if err != nil {
		t.Fatalf("Got an error getting openpgp notification: %v", err)
	}
	if fp != fingerprint {
		t.Errorf("Got an unexpected fingerprint: %s", fp)
	}

	err = db.ExpireOpenpgpNotifications(time.Microsecond)
	if err != nil {
		t.Fatalf("Got an error expiring openpgp notifications: %v", err)
	}

	fp, err = db.GetOpenpgpNotification(user)
	if err != nil {
		t.Fatalf("Got an error getting openpgp notification: %v", err)
	}
	if fp != "" {
		t.Errorf("Got an unexpected fingerprint: %s", fp)
	}
}
