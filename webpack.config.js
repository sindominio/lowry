var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const path = require('path');

module.exports = {
  entry: [
    'jquery',
    './assets/index.js',
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      // https://github.com/webpack-contrib/extract-text-webpack-plugin#extracting-sass-or-less
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      },
      //https://webpack.js.org/loaders/file-loader/
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/',
            },
          },
        ],
      },
      //https://github.com/smooth-code/svgr/tree/master/packages/webpack
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        issuer: {
          test: /\.jsx?$/
        },
        use: ['babel-loader', '@svgr/webpack', 'file-loader']
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'img/',
        }, 
      },
      {
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },
          {
            loader: 'expose-loader',
            options: '$'
          }]
      }
    ]
  },
  plugins: [
    require('autoprefixer'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    }),
    new ExtractTextPlugin('style.css')
  ]
};



//
//
//
//https://getbootstrap.com/docs/4.0/getting-started/webpack/
//    loaders: [
//      {
//        test: /\.scss$/,
//        use: [{
//          loader: 'style-loader', // inject CSS to page
//        }, {
//          loader: 'css-loader', // translates CSS into CommonJS modules
//        }, {
//          loader: 'postcss-loader', // Run post css actions
//          options: {
//            plugins: function () { // post css plugins, can be exported to postcss.config.js
//              return [
//                require('precss'),
//                require('autoprefixer')
//              ];
//            }
//          }
//        }, {
//          loader: 'sass-loader' // compiles Sass to CSS
//        }]
//      }
//    ],
