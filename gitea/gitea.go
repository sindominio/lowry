package gitea

import (
	"code.gitea.io/sdk/gitea"
	"fmt"
	"log"
	"net/url" // to check URL init vars
	"reflect" // to compare structs
)

// Gitea holds the gitea functionality config vars
type Gitea struct {
	giteaURL          string
	token             string
	cloneAddr         string
	webhookRepoURL    string
	webhookRepoSecret string
	webhookURL        string
	webhookSecret     string
}

type Repo struct {
	user              string
	repo              string
	cloneAddr         string
	webhookRepoURL    string
	webhookRepoSecret string
	client            *gitea.Client
}

func isValidURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	return err == nil
}

// Init gitea config vars
func Init(giteaURL, token, cloneAddr, webhookRepoURL, webhookRepoSecret, webhookURL, webhookSecret string) *Gitea {
	// check URL values
	if !isValidURL(giteaURL) || !isValidURL(cloneAddr) || !isValidURL(webhookRepoURL) || !isValidURL(webhookURL) {
		giteaURL = "" // to avoid process
		log.Println("Some config URL fields are not valid.")
	}

	return &Gitea{
		giteaURL:          giteaURL,
		token:             token,
		cloneAddr:         cloneAddr,
		webhookRepoURL:    webhookRepoURL,
		webhookRepoSecret: webhookRepoSecret,
		webhookURL:        webhookURL,
		webhookSecret:     webhookSecret,
	}

}

func (g *Gitea) Repo(user string, repo string) (*Repo, error) {
	if g.giteaURL == "" || g.token == "" || g.webhookRepoSecret == "" || g.webhookSecret == "" {
		return nil, fmt.Errorf("Gitea config isn't right, some fields are empty.")
	}
	client, err := gitea.NewClient(g.giteaURL, gitea.SetToken(g.token))
	if err != nil {
		return nil, err
	}
	return &Repo{
		user:              user,
		repo:              repo,
		cloneAddr:         g.cloneAddr,
		webhookRepoURL:    g.webhookRepoURL,
		webhookRepoSecret: g.webhookRepoSecret,
		client:            client,
	}, nil
}

// UserID check users exists and return user id
func (r *Repo) UserID() (int, error) {
	searchUsersOption := gitea.SearchUsersOption{
		KeyWord: r.user,
	}
	userexist, _, err := r.client.SearchUsers(searchUsersOption)
	if err != nil {
		return -1, err
	}

	uid := -2
	emptyuser := []*gitea.User{}
	if reflect.DeepEqual(userexist, emptyuser) == true {
		return uid, err
	}
	for i := 0; i < len(userexist); i++ {
		if userexist[i].UserName == r.user {
			uid = int(userexist[i].ID)
		}
	}
	return uid, nil
}

// Exists checks if the repo exists.
func (r *Repo) Exists() bool {
	repoexists, _, err := r.client.GetRepo(r.user, r.repo)
	if err != nil && err.Error() != "404 Not Found" {
		return false
	}
	// (API responses with empty repo if doesn't exist.)
	emptyrepo := &gitea.Repository{}
	return !reflect.DeepEqual(repoexists, emptyrepo)
}

// Delete the repo
func (r *Repo) Delete() error {
	_, err := r.client.DeleteRepo(r.user, r.repo)
	return err
}

// Migrate template repo to new user repo
func (r *Repo) Migrate() error {
	migrateRepoOption := gitea.MigrateRepoOption{
		CloneAddr:   r.cloneAddr,
		Description: "Basic hugo site repo",
		RepoOwner:   r.user,
		RepoName:    r.repo,
		Private:     true}

	_, _, err := r.client.MigrateRepo(migrateRepoOption)
	return err
}

// DeployKey put a deploy key on the user repo
func (r *Repo) DeployKey(keypub string) error {
	deployKeyOption := gitea.CreateKeyOption{
		Key:      keypub,
		ReadOnly: true,
		Title:    "Web deploy key"}
	_, _, err := r.client.CreateDeployKey(r.user, r.repo, deployKeyOption)
	return err
}

// CreateWebhook create a webhook on the user repo
func (r *Repo) CreateWebhook() error {
	webHookOption := gitea.CreateHookOption{
		Active: true,
		Config: map[string]string{"content_type": "json",
			"url":    r.webhookRepoURL,
			"secret": r.webhookRepoSecret},
		Events: []string{"push"},
		Type:   "gitea"}

	_, _, err := r.client.CreateRepoHook(r.user, r.repo, webHookOption)
	// TODO: <nil> no vale pa na
	if err != nil && err.Error() != "<nil>" {
		log.Printf("API Error on migrate.")
		return err
	}
	return nil

}

// Get repo info
func (r *Repo) GetSSHURL() (string, error) {
	createdRepo, _, err := r.client.GetRepo(r.user, r.repo)
	// TODO: <nil> no vale pa na
	if err != nil {
		log.Printf("Error on getting info from new created Repo.")
		return "", err
	}
	return createdRepo.SSHURL, nil

}
